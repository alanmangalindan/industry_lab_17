package ictgradschool.industry.lab17;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class AssertionsTest {

    private int x, y, z;
    String a, b, c, d;

    @Before
    public void setUp() {
        x = 10;
        y = 10;
        z = 100;
        a = new String("#");
        b = new String("$");
        c = a;
        d = new String("#");
    }

    @Test
    public void testAssert() {
        assertEquals(10, x);
        // assertEquals(x, z); // this evaluates to False
        assertFalse(x == z);
        assertTrue(Math.sqrt(z) == x);
        // assertFalse(x == y); // this evaluates to FAlse
        assertTrue(x == y);
        assertTrue(x == 10);
    }

    @Test
    public void testAssert2() {
//        assertEquals(a, b); // # != $
        assertEquals(a, c);
        assertEquals(a, d);
//        assertSame(a, b); // a and b are pointing to different objects
        assertSame(a, c);
    }

}
